This project is a demonstration of Support contrib semver releases
(https://www.drupal.org/project/drupalorg/issues/2681459), and various release
version numbers which Drupal.org will provide. It also is used by Drush to test
its pm:security command (thus some releases are marked insecure).

Not for use on real sites!
